#!/bin/sh

if [ ! -f "/opt/otrs/installed" ]  ; then
useradd -d /opt/otrs -c 'OTRS user' otrs
usermod -a -G www-data otrs
cp /opt/otrs/Kernel/Config.pm.dist /opt/otrs/Kernel/Config.pm
/opt/otrs/bin/otrs.SetPermissions.pl  --otrs-user=otrs   --web-group=www-data /opt/otrs
ln -s /opt/otrs/scripts/apache2-httpd.include.conf /etc/apache2/sites-available/otrs.conf
a2ensite otrs
echo "ServerName localhost" >> /etc/apache2/apache2.conf
service apache2 start
service mysql stop
usermod -d /var/lib/mysql/ mysql

echo "[mysqld_safe]
socket          = /var/run/mysqld/mysqld.sock
nice            = 0

[mysqld]
user            = mysql
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
port            = 3306
basedir         = /usr
datadir         = /var/lib/mysql
tmpdir          = /tmp
lc-messages-dir = /usr/share/mysql
skip-external-locking
#
# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address            = 127.0.0.1
#
# * Fine Tuning
#
key_buffer_size         = 16M
max_allowed_packet      = 64M
thread_stack            = 192K
thread_cache_size       = 8
# This replaces the startup script and checks MyISAM tables if needed
# the first time they are touched
myisam-recover-options  = BACKUP
#max_connections        = 100
#table_cache            = 64
#thread_concurrency     = 10
#
# * Query Cache Configuration
#
collation-server = utf8_unicode_ci
init-connect='SET NAMES utf8'
character-set-server = utf8

innodb_log_file_size=512MB
query_cache_limit       = 1M
query_cache_size        = 64M
#
# * Logging and Replication
#
# Both location gets rotated by the cronjob.
# Be aware that this log type is a performance killer.
# As of 5.1 you can enable the log at runtime!
#general_log_file        = /var/log/mysql/mysql.log
#general_log             = 1
#
# Error log - should be very few entries.
#
log_error = /var/log/mysql/error.log
#
# Here you can see queries with especially long duration

expire_logs_days        = 10
max_binlog_size   = 100M" > /etc/mysql/mysql.conf.d/mysqld.cnf

service mysql restart

# mysql -uroot -B9De48ycL1vvwYtl otrs < /opt/otrs/backupbanco.sql

# service mysql restart

cd /opt/otrs/var/cron

for foo in *.dist; do cp $foo `basename $foo .dist`; done

su -c "/opt/otrs/bin/Cron.sh start" -s /bin/bash otrs

# su -c "/opt/otrs/bin/otrs.Daemon.pl start" -s /bin/bash otrs

su -c "/opt/otrs/bin/otrs.Daemon.pl stop >> /dev/null" -s /bin/bash otrs
su -c "/opt/otrs/bin/otrs.Daemon.pl start >> /dev/null" -s /bin/bash otrs

touch "/opt/otrs/installed"
fi
# service apache2 start
# service mysql start
/usr/bin/supervisord

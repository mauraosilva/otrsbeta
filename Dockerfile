FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y \
    apt-utils \
    apache2 \
    git \
    bash-completion \
    cron \
    sendmail \
    curl \
    vim \
    wget \
    mysql-client   

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y mysql-server

RUN apt-get install -y \
    libdbd-mysql-perl \
    libtimedate-perl \
    supervisor \
    libnet-dns-perl \
    libio-socket-ssl-perl \
    libpdf-api2-perl \
    libsoap-lite-perl \
    libtext-csv-xs-perl \
    libjson-xs-perl \
    libapache-dbi-perl \
    libxml-libxml-perl \
    libxml-libxslt-perl \
    libyaml-perl \
    libarchive-zip-perl \
    libcrypt-eksblowfish-perl \
    libencode-hanextra-perl \
    libmail-imapclient-perl \
    libtemplate-perl \
    libterm-readline-perl-perl \
    locales \
    libcrypt-ssleay-perl \
    libdatetime-perl \
    libdbi-perl \
    libdbd-odbc-perl \
    libdbd-pg-perl \
    libauthen-sasl-perl \
    libauthen-ntlm-perl \
    libxml-parser-perl \
    libyaml-libyaml-perl

RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY daemonstarter.sh /opt/otrs/
RUN chmod +x /opt/otrs/daemonstarter.sh
RUN echo "* * * * * /opt/otrs/daemonstarter.sh" | crontab -

COPY backupbanco.sh /opt/otrs/
RUN chmod +x /opt/otrs/backupbanco.sh
RUN echo "0 1 * * * /opt/otrs/backupbanco.sh" | crontab -


COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh
EXPOSE 80
# ENTRYPOINT ["/entrypoint.sh"]
CMD ["/entrypoint.sh"]